#include "counting.h"
#include <cstdio>
#include <cassert>
#include <thrust/scan.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/device_ptr.h>
#include <thrust/execution_policy.h>
#include <device_launch_parameters.h>

__device__ __host__ int CeilDiv(int a, int b) { return (a-1)/b + 1; }
__device__ __host__ int CeilAlign(int a, int b) { return CeilDiv(a, b) * b; }

template<typename T>
struct MyStruct
{
	__host__ __device__ 
		bool operator()(const T &lhs, const T &rhs) const{	return rhs != '\n';}

};

void CountPosition1(const char *text, int *pos, int text_size)
{
	thrust::device_ptr<char> tex_ptr((char*)text);
	thrust::device_ptr<int> tex_pos((int*)pos);
	thrust::fill(tex_pos, tex_pos + text_size, 1);
	thrust::device_ptr<char> S = thrust::find(tex_ptr, tex_ptr + text_size, '\n');	
	MyStruct<char>binary_pred;
	thrust::exclusive_scan_by_key(S, tex_ptr + text_size, tex_pos + (S - tex_ptr), tex_pos + (S - tex_ptr), 0, binary_pred);
	thrust::inclusive_scan_by_key(tex_ptr, S, tex_pos, tex_pos, binary_pred);
}

__global__ void Count(const char *text, int *pos, int text_size)
{

	const int y = blockIdx.y * blockDim.y + threadIdx.y;
	const int x = blockIdx.x * blockDim.x + threadIdx.x;

	int a = y * 6720 + x;

	int idx = a, count = 0;
	if (a < text_size)
	{
		while (text[idx] != '\n' && idx>=0)
		{			
			idx--;
			count++;
		}
		pos[a] = count;
	}
	
//
//	//for (int i = 0; i < text_size; i++)
//	//{
//	//	if (text[i] == '\n')
//	//	{
//	//		count = 0;
//	//		pos[i] = count;
//	//		//std::cout << posi[i];
//	//		//std::cout << posi[i];			
//	//	}
//	//	else
//	//	{
//	//		count += 1;
//	//		pos[i] = count;
//	//		//std::cout << posi[i];
//	//		//std::cout << posi[i];			
//	//	}
//	//}
}


void CountPosition2(const char *text, int *pos, int text_size)
{
	//int count  = 0;

	//char *tex = (char*)malloc(text_size* sizeof(char)) ;
	//int *posi = (int*)malloc(text_size* sizeof(int)) ;

	//cudaMemcpy(tex,(char*)text, text_size*sizeof(char), cudaMemcpyDeviceToHost);
	//cudaMemcpy(posi, pos, text_size*sizeof(int), cudaMemcpyDeviceToHost);
	//
	//for (int i = 0; i < text_size; i++)
	//{
	//	if (tex[i] == '\n')
	//	{
	//		count = 0;
	//		posi[i] = count;
	//		//std::cout << posi[i];
	//		//std::cout << posi[i];			
	//	}
	//	else
	//	{
	//		count += 1;
	//		posi[i] = count;
	//		//std::cout << posi[i];
	//		//std::cout << posi[i];			
	//	}
	//}

	//cudaMemcpy((char*)text, tex, text_size * sizeof(char), cudaMemcpyHostToDevice);
	//cudaMemcpy(pos, posi, text_size * sizeof(int), cudaMemcpyHostToDevice);

	dim3 block = dim3(240, 240);
	dim3 thread = dim3(28, 28);
	

	Count <<<block, thread >>> (text, pos, text_size);

}

