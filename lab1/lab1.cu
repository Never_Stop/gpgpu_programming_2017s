﻿#include "lab1.h"
#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <curand.h>
#include <curand_kernel.h>
#include "SyncedMemory.h"
#include "lab1.h"

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
static const unsigned W = 640;
static const unsigned H = 480;
static const unsigned NFRAME = 240;

struct Lab1VideoGenerator::Impl {
	int t = 0;
};
Lab1VideoGenerator::Lab1VideoGenerator(): impl(new Impl) {}

Lab1VideoGenerator::~Lab1VideoGenerator() {}


__device__ int T = 20;
__device__ int speed = 10;
__device__ bool a = true;
__device__ int colr = 0;
__device__ void pixel(uint8_t *yuv, int x, int y, int r, int g, int b) {
	// y
	yuv[y * W + x] = 0.299 * r + 0.587 * g + 0.114 * b;

	if (y % 2 == 0 && x % 2 == 0) {
		int offset = y * W / 4 + x / 2;
		// u
		yuv[W * H + offset] = -0.169 * r - 0.331 * g + 0.500 * b + 128;
		// v
		yuv[W * H * 5 / 4 + offset] = 0.500 * r - 0.419 * g - 0.081 * b + 128;
	}
}

__global__ void Draw(uint8_t *yuv) {
	 //TODO: draw more complex things here
	 //Do not just submit the original file provided by the TA!
	const int row = blockIdx.y * blockDim.y + threadIdx.y;
	const int col = blockIdx.x * blockDim.x + threadIdx.x;


	int idx = row * W + col;
	if (col >= W || row >= H) return;

	float x0 = ((float)col / W) * 3.5f - 2.5f;
	float y0 = ((float)row / H) * 3.5f - 1.75f;

	float x = 0.0f;
	float y = 0.0f;
	int iter = 0;
	float xtemp;
	while ((x * x + y * y <= 4.0f) && (iter <= T))
	{
		xtemp = x * x - y * y + x0;
		y = 2.0f * x * y + y0;
		x = xtemp;
		iter++;
	}

	int color = iter ;
	float norm = (float)iter / (float)T;
	
	//pixel(yuv, col, row, 128 * norm, 64 * norm, norm * 255);
	pixel(yuv, col, row,  norm, (255- colr) * norm, norm * colr);
	//pixel(yuv, col, row, 255 * norm, 181 * norm, norm * 5);
	
	if (row * W + col == 333)
	{
		if (a)
		{
			T += speed;
			colr += speed;
		}
		else
		{
			T -= speed;
			colr -= speed;
		}
		if(T >= 260)
		{
			a = false;
			T = 260;
		}
		if (T <= 20) {
			a = true;
			T = 20;
		}
	}
}

void Lab1VideoGenerator::get_info(Lab1VideoInfo &info) {
	info.w = W;
	info.h = H;
	info.n_frame = NFRAME;
	// fps = 24/1 = 24
	info.fps_n = 24;
	info.fps_d = 1;
};


void Lab1VideoGenerator::Generate(uint8_t *yuv) {

	dim3 grid = dim3(20, 20);
	dim3 block = dim3(32, 24);	

	Draw <<<grid, block>>>(yuv);

}

