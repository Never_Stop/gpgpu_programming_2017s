#include "lab3.h"
#include <cstdio>
#include "SyncedMemory.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"


__device__ __host__ int CeilDiv(int a, int b) { return (a-1)/b + 1; }
__device__ __host__ int CeilAlign(int a, int b) { return CeilDiv(a, b) * b; }


__device__ bool readcolor(const float *clr_now, int w, int h, int x, int y, float *new_rgb)
{
	if (x >= 0 && x < w && y >= 0 && y < h)
	{
		int idx = (y * w + x) * 3;
		new_rgb[0] = clr_now[idx];
		new_rgb[1] = clr_now[idx + 1];
		new_rgb[2] = clr_now[idx + 2];

		return true;
	}
	return false;
}

__global__ void CalculateFixed(
	const float *background, const float *target, const float *mask, float *output,
	const int wb, const int hb, const int wt, const int ht, const int oy, const int ox)
{
	const int yt = blockIdx.y * blockDim.y + threadIdx.y;
	const int xt = blockIdx.x * blockDim.x + threadIdx.x;
	const int curt = wt*yt + xt;
	if (yt < ht && xt < wt && mask[curt] > 127.0f) {
		
		float now_rgb[3];
		float fix_rgb[3] = { 0,0,0 };
		
		readcolor(target, wt, ht, xt, yt, now_rgb);

		int dir_X[4] = { -1,0,1,0 };
		int dir_Y[4] = { 0,1,0,-1 };

		for (int i = 0; i < 4; i++)
		{
			int new_xt = xt + dir_X[i];
			int new_yt = yt + dir_Y[i];

			float new_rgb[3];
			bool in = readcolor(target, wt, ht, new_xt, new_yt, new_rgb);
			if (in)
			{
				fix_rgb[0] = fix_rgb[0] + now_rgb[0] - new_rgb[0];
				fix_rgb[1] = fix_rgb[1] + now_rgb[1] - new_rgb[1];
				fix_rgb[2] = fix_rgb[2] + now_rgb[2] - new_rgb[2];
			}
			if (!in || mask[new_yt*wt + new_xt] <= 127)
			{
				float backg_rgb[3];				
				int new_xb = new_xt + ox;
				int new_yb = new_yt + oy;
				if (readcolor(background, wb, hb, new_xb, new_yb, backg_rgb))
				{
					fix_rgb[0] = fix_rgb[0] + backg_rgb[0];
					fix_rgb[1] = fix_rgb[1] + backg_rgb[1];
					fix_rgb[2] = fix_rgb[2] + backg_rgb[2];
				}
			}
		}

		output[curt * 3 + 0] = fix_rgb[0];
		output[curt * 3 + 1] = fix_rgb[1];
		output[curt * 3 + 2] = fix_rgb[2];
	}
}

__global__ void jacobi(
	const float *background,const float *target,const float *fixed,const float *mask,float *output,
	const int wb, const int hb, const int wt, const int ht,const int oy, const int ox)
{
	const int yt = blockIdx.y * blockDim.y + threadIdx.y;
	const int xt = blockIdx.x * blockDim.x + threadIdx.x;
	const int curt = wt*yt + xt;
	if (yt < ht && xt < wt)
	{
		if (mask[curt] > 127.0f)
		{
			float fix_rgb[3];
			readcolor(fixed, wt, ht, xt, yt, fix_rgb);

			int dir_X[4] = { -1, 0, 1, 0 };
			int dir_Y[4] = { 0, 1, 0, -1 };
			for (int i = 0; i < 4; ++i)
			{
				int new_xt = xt + dir_X[i];
				int new_yt = yt + dir_Y[i];
				if (new_yt >= 0 && new_yt < ht && new_xt >= 0 && new_xt < wt)
				{
					if (mask[new_yt * wt + new_xt] > 127)
					{
						float backg_rgb[3];
						int new_yb = new_yt + oy;
						int new_xb = new_xt + ox;
						if (readcolor(target, wt, ht, new_xt, new_yt, backg_rgb))
						{
							fix_rgb[0] = fix_rgb[0] + backg_rgb[0];
							fix_rgb[1] = fix_rgb[1] + backg_rgb[1];
							fix_rgb[2] = fix_rgb[2] + backg_rgb[2];
						}
					}
				}
			}

			output[curt * 3 + 0] = fix_rgb[0] / 4;
			output[curt * 3 + 1] = fix_rgb[1] / 4;
			output[curt * 3 + 2] = fix_rgb[2] / 4;
		}
		else
		{
			const int yb = oy + yt, xb = ox + xt;
			const int curb = wb*yb + xb;
			output[curt * 3 + 0] = background[curb * 3 + 0];
			output[curt * 3 + 1] = background[curb * 3 + 1];
			output[curt * 3 + 2] = background[curb * 3 + 2];
		}
	}
}

__global__ void SimpleClone(
	const float *background,
	const float *target,
	const float *mask,
	float *output,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox
)
{
	const int yt = blockIdx.y * blockDim.y + threadIdx.y;
	const int xt = blockIdx.x * blockDim.x + threadIdx.x;
	const int curt = wt*yt + xt;
	if (yt < ht && xt < wt && mask[curt] > 127.0f) {
		const int yb = oy + yt, xb = ox + xt;
		const int curb = wb*yb + xb;
		if (0 <= yb && yb < hb && 0 <= xb && xb < wb) {
			output[curb * 3 + 0] = target[curt * 3 + 0];
			output[curb * 3 + 1] = target[curt * 3 + 1];
			output[curb * 3 + 2] = target[curt * 3 + 2];
		}
	}
}

void PoissonImageCloning(
	const float *background,
	const float *target,
	const float *mask,
	float *output,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox
)
{
	// set up
	float *fixed, *buf1, *buf2;
	cudaMalloc(&fixed, 3 * wt*ht * sizeof(float));
	cudaMalloc(&buf1, 3 * wt*ht * sizeof(float)); 
	cudaMalloc(&buf2, 3 * wt*ht * sizeof(float)); 

	CalculateFixed <<<dim3(CeilDiv(wt, 32), CeilDiv(ht, 16)), dim3(32, 16) >>> (
		background, target, mask, fixed,
		wb, hb, wt, ht, oy+10, ox+600
		);
	cudaMemcpy(buf2, target, sizeof(float) * 3 * wt*ht, cudaMemcpyDeviceToDevice);

	// iterate
	for (int i = 0; i < 20000; ++i) {
		jacobi <<<dim3(CeilDiv(wt, 32), CeilDiv(ht, 16)), dim3(32, 16) >>> (
			background, buf2, fixed, mask, buf1,
			wb, hb, wt, ht, oy+10, ox+600
			);
		jacobi <<<dim3(CeilDiv(wt, 32), CeilDiv(ht, 16)), dim3(32, 16) >>> (
			background, buf1, fixed, mask, buf2,
			wb, hb, wt, ht, oy+10, ox+600
			);

	}

	// copy the image back
	cudaMemcpy(output, background, wb*hb * sizeof(float) * 3, cudaMemcpyDeviceToDevice);
	SimpleClone <<<dim3(CeilDiv(wt, 32), CeilDiv(ht, 16)), dim3(32, 16) >>> (
		background, buf2, mask, output,
		wb, hb, wt, ht, oy+10, ox+600
		);

	// clean up
	cudaFree(fixed);
	cudaFree(buf1);
	cudaFree(buf2);
}
